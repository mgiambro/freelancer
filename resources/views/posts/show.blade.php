
@extends('layouts.master')

@section('content')
<div class="blog-header"  style="padding-top: 3rem!important;">
  <div class="container">
    <h1 class="blog-title">{{ $post->title }}</h1>
  </div>
</div>
<main role="main" class="container">
  <div class="row">
      <div class="col-sm-8 blog-main">

          {!! $post->body !!}
          @unless ($post->tags->isEmpty())
          <h5>Tags:</h5>
          <ul>
              @foreach($post->tags as $tag)
              <li>{{ $tag->name }}</li>
              @endforeach
          </ul>
          @endunless
      </div>
          @include('layouts.sidebar')
    </div><!-- /.row -->
</main><!-- /.container -->

@endsection
