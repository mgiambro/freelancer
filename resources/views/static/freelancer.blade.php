@extends('layouts.master')

@section('content')
    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
        <div class="my-auto">
          <h1 class="mb-0">Maurizio
            <span class="text-primary">Giambrone</span>
          </h1>
          <div class="subheading mb-5">Bournemouth, UK ·
            <a href="{{ route('posts.index') }}">PHP Web Developer</a>
          </div>
          <p class="mb-5">
              I am a freelance web developer offering a range of php programming services that you may require as part of a website build or as ongoing support for your day to day business requirements. A custom website project for example may need some php progamming for some bespoke functionality, such as a members area for example. If you require a php developer or php developers, I would be happy to quote you for your project and offer php programming & mysql database solutions to suit all budgets.
          </p>
          <ul class="list-inline list-social-icons mb-0">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="services">
        <div class="my-auto">
          <h2 class="mb-5">Services</h2>
          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Web Development Services</h3>
              <p>As a freelance web developer I offer web development services for websites, database backends and bespoke coded projects.</p>
                <p>
                  Depending on the project requirements, I use either the Laravel PHP framework or a component based approach where the application is built from scratch using a mixture of
                  hand-coded PHP and carefully selected components for common web application functions such as routing, database access and logging.
                </p>
                <p>
                  As well creating web applications for clients that contact me directly I can also offer my services on an outsourcing basis to other design agencies and php developers
                  who need additional resources. If you perhaps have a requirement for professional web development services or php programming from a php web developer like
                  myself, then please contact me under no obligation. Remember, my web development services are also cost effective and affordable.
                </p>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Domain Names &amp; Web Hosting Services</h3>
              <p>
                  Domain name registration and web hosting services are also available.
              </p>
              <p>
                  I will carry out a domain name search, register that domain, source the most appropriate web hosting package for your application and
                  deploy it.
              </p>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Website Maintenance</h3>
              <p>
                  Updating your website is as important as building it, and for two main reasons. First, web technologies change with time and some features
                  built into your existing website might become obsolete. Secondly, your brand image suffers if you present the same website to your customers
                  and fail to evolve with time. That's where I can help with my website management services.
              </p>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Website Support Services</h3>
              <p>
                Once your new website is up and running you need to know that the company you have chosen to create your website will still be there to provide you with
                backup and website design support services, should you have a problem or require an alteration for your site.
              </p>
              <p>
                I provide affordable website design support services on an ongoing basis whether I created your website, or if someone else designed it for you.
                I can investigate issues, fix errors and offer advice and maintainence on an ongoing monthly basis.
              </p>
            </div>
          </div>

        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="skills">
        <div class="my-auto">
          <h2 class="mb-5">Skills</h2>

          <div class="subheading mb-3">Programming Languages &amp; Tools</div>
          <ul class="list-inline list-icons">
            <li class="list-inline-item">
              <i class="devicons devicons-php"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-laravel"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-mysql"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-amazonwebservices"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-linux"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-ssh"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-ubuntu"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-java"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-atom"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-git"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-bitbucket"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-html5"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-css3"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-javascript"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-jquery"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-sass"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-bootstrap"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-webpack"></i>
            </li>
            <li class="list-inline-item">
              <i class="devicons devicons-npm"></i>
            </li>
          </ul>

          <div class="subheading mb-3">Workflow</div>
          <ul class="fa-ul mb-0">
            <li>
              <i class="fa-li fa fa-check"></i>
              Object Oriented Design</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Test Driven Development</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Cross Functional Teams</li>
            <li>
              <i class="fa-li fa fa-check"></i>
              Agile Development &amp; Scrum</li>
          </ul>
        </div>
      </section>

    </div>

@endsection
