@extends('layouts.master')

@section('content')
<div class="col-sm-8 blog-main" style="padding-top: 3rem!important;">
  <h1>Contact Me</h1>

  @include('layouts.errors')
  @include('flash::message')
  {!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}

  <div class="form-group">
      {!! Form::label('Your Name') !!}
      {!! Form::text('name', null,
          array('required',
                'class'=>'form-control',
                'placeholder'=>'Your name')) !!}
  </div>

  <div class="form-group">
      {!! Form::label('Your E-mail Address') !!}
      {!! Form::text('email', null,
          array('required',
                'class'=>'form-control',
                'placeholder'=>'Your e-mail address')) !!}
  </div>

  <!-- Spam trap -->
  {!! Form::text('url', null,array('class'=>'antispam')) !!}
  <!-- End spam trap -->
  
  <div class="form-group">
      {!! Form::label('Your Message') !!}
      {!! Form::textarea('message', null,
          array('required',
                'class'=>'form-control',
                'placeholder'=>'Your message')) !!}
  </div>

  <div class="form-group">
      {!! Form::submit('Contact Me!',
        array('class'=>'btn btn-primary')) !!}
  </div>

  {!! Form::close() !!}

</div>
@endsection
