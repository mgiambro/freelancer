
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="../../../../favicon.ico">

        <title>Blog</title>

        <!-- Custom styles for this template -->
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>

    <body>
      <!-- Putting scripts here as they don't worj in the footer using laravel mix. -->
      <script src="{{ mix('js/manifest.js') }}"></script>
      <script src="{{ mix('js/vendor.js') }}"></script>
      <script src="{{ mix('js/app.js') }}"></script>

      @include('layouts.nav')

        @if ($flash = session('message'))
        <div id="flash-message" class="alert alert-success" role="alert">
            {{ $flash }}
        </div>
        @endif

        @if (Auth::check())
        <div class="blog-header">
            <div class="container">
                <h1 class="blog-title">The Bootstrap Blog</h1>
                <p class="lead blog-description">An example blog template built with Bootstrap.</p>
            </div>
        </div>
        @endif

        <div class="container">

            <div class="row">
                @include('flash::message')
                @yield('content')

                @if (Auth::check())
                  @include('layouts.sidebar')
                @endif
              </div><!-- /.row -->

          </div><!-- /.container -->

          @include('layouts.footer')
</html>
