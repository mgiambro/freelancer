<?php
//Auth::routes();

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
// Route::get('logout', function(){
//   echo 'Boo';
// });

// Registration Routes...
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// Posts
// Route::get('/projects', 'PostsController@index')->name('projects');
//
// Route::get('/posts/create', 'PostsController@create');
//
// Route::post('/posts', 'PostsController@store');
//
// Route::get('/posts/{post}', 'PostsController@show');
//
// Route::get('posts/{id}/edit', 'PostsController@edit');
//


Route::resource('posts', 'PostsController');
//Route::get('/projects', 'PostsController@index')->name('projects');
Route::view('/freelancer', 'static.freelancer');

Route::get('/',['as'=>'home', function(){
  return view('static.freelancer');
}]);

Route::get('contact',
  ['as' => 'contact', 'uses' => 'ContactController@create']);
Route::post('contact',
  ['as' => 'contact_store', 'uses' => 'ContactController@store']);

Route::get('/posts/tags/{tag}', 'TagsController@show');

// Route::get('/freelancer', function(){
// return 'Hello World';
// });

// Route::get('/experience',['as'=>'experience', function(){
//   return view('static.experience');
// }]);
