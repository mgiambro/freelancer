<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\PostRepositoryInterface;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

/**
 * Description of ArticleRepository
 *
 * @author maurizio
 */
class PostRepository implements PostRepositoryInterface {

    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function latest()
    {
        return $this->post->latest()->first();
    }

    // Returns a collection object
    public function all()
    {
        return $this->post->latest('published_at')->published()->get(); // Uses query scopePublished on Post model.
    }

    // Returns a Model object
    public function allModel()
    {
        return $this->post->latest('published_at')->published(); // Uses query scopePublished on Post model.
    }

    public function create(array $input)
    {
        return  $this->createPost($input);
    }

    public function delete($id)
    {
        return $this->post->destroy($id);
    }

    public function find($id, $columns = array('*'))
    {
        return $this->post->findOrFail($id, $columns);
    }

    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->post->where($attribute, '=', $value)->first($columns);
    }

    public function paginate($perPage = 15, $columns = array('*'))
    {
        return $this->post->paginate($perPage, $columns);
    }

    public function update(Post $post, array $data, $id = null)
    {
        $this->syncTags($post, $data['tag_list']);
        return $post->update($data); // with route-model binding
    }

    private function syncTags(Post $post, $tagList)
    {
        $post->tags()->sync($tagList);
    }

    private function createPost(array $input)
    {
        $post = Auth::user()->posts()->create($input);

        if(array_key_exists('tag_list', $input)){
            $tagIds = $input['tag_list'];
            $this->syncTags($post, $tagIds);
        }

        return $post;
    }

}
