<?php

namespace App\Repositories\Contracts;

use App\Models\Post;

/**
 *
 * @author maurizio
 */
interface PostRepositoryInterface {

    public function all();

    public function paginate($perPage = 15, $columns = array('*'));

    public function create(array $input);

    public function update(Post $post, array $data, $id = null);

    public function delete($id);

    public function find($id, $columns = array('*'));

    public function findBy($field, $value, $columns = array('*'));

    public function latest();

}
