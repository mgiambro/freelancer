<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{
    public function create()
    {
        return view('contact.contact');
    }

    public function store(ContactFormRequest $request)
    {
      $spam = $request->get('url');
      if($spam==null || $spam=='' ){

          \Mail::send('emails.contact',
            array(
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'user_message' => $request->get('message')
        ), function($message)
        {
            $message->from('mgiambrone.dev@gmail.com');
            $message->to('mgiambrone.dev@gmail.com', 'Admin')->subject('Development Enquiry');
        });
          flash('Thanks for contacting me. I will respond as soon as I can.')->success();
          return \Redirect::route('contact')
        ->with('message', 'Thanks for contacting me!');
        }

    }
}
