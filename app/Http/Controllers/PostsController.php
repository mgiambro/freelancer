<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\Contracts\PostRepositoryInterface;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Repositories\Contracts\TagRepositoryInterface;

class PostsController extends Controller
{
  protected $postRepo;
  protected $tagRepo;

    public function __construct(PostRepositoryInterface $postRepo, TagRepositoryInterface $tagRepo) {
        $this->postRepo = $postRepo;
        $this->tagRepo = $tagRepo;
        $this->middleware('auth')->except(['index', 'show']);
        $this->middleware('admin')->only(['create','edit']);
    }

    public function index() {

        $posts = $this->postRepo->allModel();

        if (request(['month', 'year'])) {
            $posts->filter(request(['month', 'year']));
        }

        $posts = $posts->get();

        return view('posts.index', compact('posts', 'archives'));
    }

    public function show(Post $post) {
        // Traditional
        // $post = Post::find($id); // need $id arg in the show method declaration
        // Route Model Binding to Post arg
        return view('posts.show', compact('post'));
    }

    public function create() {
        $tags = $this->tagRepo->all();
        return view('posts.create', compact('tags'));
    }

    public function store(PostRequest $request) {
        $input = $request->all();

        $this->postRepo->create($input);

    //    flash()->overlay('Your post has been created', 'Good Job');
        flash('Your post has been created', 'Good Job')->success();
        return redirect('posts');
    }

    public function edit(Post $post)
    {
        $tags = $this->tagRepo->all();
        return view('posts.edit', compact('post', 'tags'));
    }

    // Using Route-Model Binding - Use for api's and small projects
    public function update(Post $post, PostRequest $request)
    {
        $input = $request->all();
        $this->postRepo->update($post, $input);

        return redirect('posts');
    }
}
