<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class TagsController extends Controller {

    public function show(Tag $tag)
    {
        $posts = $tag->posts()->published()->get();
        
        return view('posts.index', compact('posts'));
    }

}
