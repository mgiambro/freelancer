<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\Tag;
use App\User;

class Post extends Model
{
      protected $fillable = ['title',
          'body',
          'published_at']; // Whitelist

      protected $dates = ['published_at'];

      // Relationships
      public function user() {
          return $this->belongsTo(User::class);
      }

      /*
       * Get the tags associated with the given article
       */
      public function tags()
      {
          return $this->belongsToMany(Tag::class)->withTimestamps();
      }

      /*
       * Get a list of tag ids associated with the current article.
       */
      public function getTagListAttribute()
      {
          return $this->tags->pluck('id')->toArray();
      }

      public function comments() {
          return $this->hasMany(Comment::class);
      }

      public function addComment($body) {
          $this->comments()->create(compact('body'));
      }

      // Query Scopes
      public function scopeFilter($query, $filters) {
    //    dd($query);
          if ($month = $filters['month']) {
              $query->whereMonth('created_at', Carbon::parse($month)->month);
          }

          if ($year = $filters['year']) {
              $query->whereYear('created_at', Carbon::parse($year)->year);
          }
      }

      public function scopePublished($query)
      {
          $query->where('published_at', '<=', Carbon::now());
      }

      public function scopeUnpublished($query)
      {
          $query->where('published_at', '>', Carbon::now());
      }

      // Mutators
      public function setPublishedAtAttribute($date)
      {
          //      $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $date);
          $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d', $date);
      }

      public function getPublishedAtAttribute($date)
      {
          return Carbon::parse($date)->format('Y-m-d H:i:s');
      }

      // Statics
      public static function archives() {
          return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) '
                          . 'published')
                  ->groupBy('year', 'month')
                  ->orderByRaw('min(created_at) desc')
                  ->get()
                  ->toArray();
      }

}
