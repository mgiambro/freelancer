<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Post;
use App\Models\Tag;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->composer('layouts.sidebar', function($view){
          $archives = Post::archives();
          $tags = Tag::has('posts')->pluck('name');

          $view->with(compact('archives','tags'));
      });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Bind interfaces to implementations
        $this->app->bind('App\Repositories\Contracts\TagRepositoryInterface', 'App\Repositories\Eloquent\TagRepository');
        $this->app->bind('App\Repositories\Contracts\PostRepositoryInterface', 'App\Repositories\Eloquent\PostRepository');
    }
}
